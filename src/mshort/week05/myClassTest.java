package mshort.week05;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;


class myClassTest {

    myClass x;

    /* This is neat, code here is performed before each Test, this handles setup of a fresh myClass object for each test */
    @BeforeEach
    public void PreTestSetup() {
        x = new myClass();
    }

    @org.junit.jupiter.api.Test
    void verifyConversionTests() {
        Assertions.assertEquals(0.0, x.convertFtoC(32.0), "Conversion of F to C is broken");
        Assertions.assertEquals(32.0, x.convertCtoF(0.0),"Conversion of C to F is broken");
        Assertions.assertEquals(-2.78, x.convertFtoC2DecimalPlaces(27.0), "Conversion of F to C with rounding to 2 decimal places is broken");
        Assertions.assertEquals(106.51, x.convertCtoF2DecimalPlaces(41.394), "Conversion of C to F with rounding to 2 decimal places is broken");
    }

    @org.junit.jupiter.api.Test
    void verifyEvenOddNumberTests() {
        int myInt = 0;
        myInt = x.getOddNumber();
        Assertions.assertFalse(x.isNumberEven(myInt), "int [" + myInt + "], is not odd or logic to detect even is flawed" );
        myInt = x.getEvenNumber();
        Assertions.assertFalse(x.isNumberOdd(myInt), "int [" + myInt + "], is not even or logic to detect odd is flawed" );
        myInt = x.getOddNumber();
        Assertions.assertTrue(x.isNumberOdd(myInt), "int [" + myInt + "], is not odd or logic to detect odd is flawed" );
        myInt = x.getEvenNumber();
        Assertions.assertTrue(x.isNumberEven(myInt), "int [" + myInt + "], is not even or logic to detect even is flawed" );
    }

    @org.junit.jupiter.api.Test
    void assertArrayEqualsTest() {
        Assertions.assertArrayEquals(new int[]{0,1,2,3,4,5,6,7,8,9}, x.getIntArray(0, 9));
    }

    @org.junit.jupiter.api.Test
    void nullStringCheck() {
        Assertions.assertNull(x.getNullString(), "Expected Null String!");
    }

    @org.junit.jupiter.api.Test
    void noNullStringCheck() {
        Assertions.assertNotNull(x.getValidString(), "Expected Not Null String!");
    }

    @org.junit.jupiter.api.Test
    void assertSameTests() {
        int[] myIntArray1 = x.getIntArray(1, 5);
        int[] myIntArray2 = x.getIntArray(1, 5);
        int[] myIntArray3 = myIntArray1;

        Assertions.assertSame(myIntArray1, myIntArray3, "myIntArray1 and myIntArray3 are not the same");
        Assertions.assertNotSame(myIntArray1, myIntArray2, "myIntArray1 and MyIntArray2 should not be the same");
    }

    @org.junit.jupiter.api.Test
    void assertThatTests() {
        MatcherAssert.assertThat("Even Number Generation or Verification is Broken", x.isNumberEven(x.getEvenNumber()), Matchers.is(true));
        MatcherAssert.assertThat("Odd Number Generation or Verification is Broken", x.isNumberOdd(x.getOddNumber()), Matchers.is(true));
        MatcherAssert.assertThat("Odd Number Generation or Even Verification is Broken", x.isNumberEven(x.getOddNumber()), Matchers.is(false));
        MatcherAssert.assertThat("Even Number Generation or Odd Verification is Broken", x.isNumberOdd(x.getEvenNumber()), Matchers.is(false));

        int[] myIntArray1 = x.getIntArray(1, 5);
        int[] myIntArray2 = x.getIntArray(1, 5);
        int[] myIntArray3 = myIntArray1;

        MatcherAssert.assertThat("myIntArray3 and myIntArray1 should reference the same instance" ,myIntArray1, Matchers.sameInstance(myIntArray3));
        MatcherAssert.assertThat("myIntArray1 and myIntArray2 should be identical", myIntArray1, Matchers.is(myIntArray2));

    }

}