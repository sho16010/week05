package mshort.week05;

import java.util.*;

public class myClass {

    /* This class will contain a bunch of invented functions that can be used to perform
       assert testing with JUnit5. They may not be that useful for other stuff, it will depend on
       how creative I get. We will include validation checking where it looks needed and exception
       catching to clue us in when things go bad and need to be fixed. Tests will be in a
       myClassTest.java file.
     */
    public double convertFtoC(double inTempF) {
        double result = 0.0;

        try {
            result = (inTempF - 32) * 5 / 9;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertFtoC:");
            System.out.println("Exception String: " + e.toString());
            System.out.println("Exception Message: " + e.getMessage());
            System.out.println("Verify inTempF Parameter Value");
            throw e;
        }

        return result;
    }

    public double convertCtoF(double inTempC) {
        double result = 0.0;

        try {
            result = ((inTempC * 9) / 5) + 32;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertCtoF:");
            System.out.println("Exception String: " + e.toString());
            System.out.println("Exception Message: " + e.getMessage());
            System.out.println("Verify inTempC Parameter Value");
            throw e;
        }

        return result;
    }

    public double convertCtoF2DecimalPlaces(double inTempC) {
        double result = 0.0;

        try {
            result = convertCtoF(inTempC);
            result = Math.round(result * 100.0) / 100.0;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertCtoF2DecimalPlaces:");
            System.out.println("Exception Message: " + e.getMessage());
        }

        return result;
    }

    public double convertFtoC2DecimalPlaces(double inTempF) {
        double result = 0.0;

        try {
            result = convertFtoC(inTempF);
            result = Math.round(result * 100.0) / 100.0;
        } catch (Exception e) {
            System.out.println("Exception encountered in convertFtoC2DecimalPlaces:");
            System.out.println("Exception Message: " + e.getMessage());
        }

        return result;
    }

    public static int getEvenNumber() {
        int result = 0;

        do {
            Random myRand = new Random();
            /* Stay at 500 or below */
            result = myRand.nextInt(500);
        } while (isNumberOdd(result));

        return result;
    }

    public static int getOddNumber() {
        int result = 1;

        do {
            Random myRand = new Random();
            /* Stay at 500 or below */
            result = myRand.nextInt(500);
        } while (isNumberEven(result));

        return result;
    }

    public static boolean isNumberEven(int inNum) {
        boolean result = false;

        if ((inNum % 2) == 0) {
            result = true;
        }
        return result;
    }

    public static boolean isNumberOdd(int inNum) {
        boolean result = false;

        if ((inNum % 2) == 1) {
            result = true;
        }
        return result;
    }

    public static String getNullString() {
        /* This is intentional for testing, we could have probably just returned null */
        String result = null;
        return result;
    }

    public static String getValidString() {
        String result = "";

        Random myRand = new Random();
        switch (myRand.nextInt(2) % 3) {
            case 0:
                result = "Hello";
                break;
            case 1:
                result = "Howdy";
                break;
            case 2:
                result = "Hi";
                break;
        }

        return result;
    }

    public static int[] getIntArray(int inLow, int inHigh) {
        int arraySize = inHigh - inLow;
        boolean errorDetected = false;
        /* We are not checking if the size is too big, we will use low numbers for testing
           We will check for sizes less than zero
        */
        if (inLow < 0 || inHigh < 0) {
            System.out.println("getIntArray: Invalid Size for inLow or inHigh, setting size to 1, and value to 0");
            arraySize = 0;
            errorDetected = true;
        } else {
            if (arraySize < 0) {
                System.out.println("getIntArray: Invalid Size [" + arraySize + "], setting size to 1, and value to 0");
                arraySize = 0;
                errorDetected = true;
            }
        }

        /* We increment the size of the array created by 1 to include the inHigh value as part of the array */
        /* If the in values were not valid, then we will make the size 1, and we will set value to 0 below */
        int[] result = new int[arraySize+1];

        try {
            if (errorDetected) {
                result[0] = 0;
            } else {
                //int currentNumber = inLow;
                for (int a = 0; a <= arraySize; a = a + 1) {
                    result[a] = inLow + a;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Array Generation Loop in getIntArray");
            System.out.println("Message: " + e.getMessage());
        }
        return result;
    }

    /* main to make sure my methods are working */
    public static void main(String[] args) {
        myClass x = new myClass();

        System.out.println("32.0 F is " + x.convertFtoC(32.0) + " C");
        System.out.println("0.0 C is " + x.convertCtoF(0.0) + " F");

        System.out.println("27.0 F is " + x.convertFtoC(27.0) + " C");
        System.out.println("41.394 C is " + x.convertCtoF(41.394) + " F");

        System.out.println("27.0 F is " + x.convertFtoC2DecimalPlaces(27.0) + " C");
        System.out.println("41.394 C is " + x.convertCtoF2DecimalPlaces(41.394) + " F");

        System.out.println("Is 5 Even? [" + x.isNumberEven(5) + "]");
        System.out.println("Is 10 Even? [" + x.isNumberEven(10) + "]");

        System.out.println("Is 5 Odd? [" + x.isNumberOdd(5) + "]");
        System.out.println("Is 10 Odd? [" + x.isNumberOdd(10) + "]");

        System.out.println("Get Even Number: [" + x.getEvenNumber() + "]");
        System.out.println("Get Odd Number: [" + x.getOddNumber() + "]");

        System.out.println("Get Valid String: [" + x.getValidString() + "]");
        System.out.println("Get Null String: [" + x.getNullString() + "]");

        System.out.println("Get IntArray 0 to 9:");
        int[] myIntArray = x.getIntArray(0,9);
        int i = 0, j = myIntArray.length;
        while (i < j) {
            System.out.println("myIntArray[" + i + "] = [" + myIntArray[i] + "]");
            i = i+1;
        }
        System.out.println("Get IntArray 5 to 13:");
        myIntArray = x.getIntArray(5,13);
        i = 0;
        j = myIntArray.length;
        while (i < j) {
            System.out.println("myIntArray[" + i + "] = [" + myIntArray[i] + "]");
            i = i+1;
        }
    }

}
